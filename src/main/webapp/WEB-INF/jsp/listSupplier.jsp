<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
		<title>List Suppliers</title>
			<style>
				table {
				  font-family: arial, sans-serif;
				  border-collapse: collapse;
				  width: 100%;
				}
				
				td, th {
				  border: 1px solid #dddddd;
				  text-align: left;
				  padding: 8px;
				}
				
				tr:nth-child(even) {
				  background-color: #dddddd;
				}
			</style>
	</head>
	<body>
		<div align="center">
			<h1>Data Suppliers</h1>
			<input type="button" value="Tambah" onclick="location.href='${pageContext.request.contextPath}/supplier/add'">
		</div>
		
		<br></br>
		
		<!-- 	Tambahan iseng tapi ga ngerti kenapa jadi	 -->
		<div align="center">
			<f:form action="${pageContext.request.contextPath}/supplier/findByName">
				<input type="text" name="parameter" onfocus="this.value=''"/>
				<input type="submit" value="Cari"/>
			</f:form>
		</div>
		
		<p><a href='${pageContext.request.contextPath}/supplier/list'>All data</a></p>
		
		<table>
		  <tr>
		    <th>Kode Supplier</th>
		    <th>Nama Supplier</th>
		    <th>Alamat</th>
		    <th>Telepon</th>
		    <th>Email</th>
		    <th>Nama Kota</th>
		    <th>Action</th>
		  </tr>
		  <c:forEach items="${list}" var = "s">
			  <tr>
			    <td>${s.kodeSupplier}</td>
			    <td>${s.namaSupplier}</td>
			    <td>${s.alamatSupplier}</td>
			    <td>${s.telpSupplier}</td>
		   	    <td>${s.emailSupplier}</td>
		   	    <td>${s.namaKota}</td>
		   	    <td>
		   	    	<input type="button" value="Edit" onclick="location.href='${pageContext.request.contextPath}/supplier/detail/${s.kodeSupplier}'">
<%-- 		   	    	<input type="button" value="Hapus" onclick="location.href='${pageContext.request.contextPath}/supplier/delete/${s.kodeSupplier}'"> --%>
					<input type="button" value="Hapus" onclick="deleteFunction('${s.kodeSupplier}')">
		   	    	<script>
						function deleteFunction(kodeSupplier) {
						  if (confirm("Anda yakin ingin menghapus data ini?")) {
							  location.href='${pageContext.request.contextPath}/supplier/delete/'+ kodeSupplier;
							  }
						}
					</script>
	   	    	</td>
			  </tr>
		  </c:forEach>
		</table>
	</body>
</html>