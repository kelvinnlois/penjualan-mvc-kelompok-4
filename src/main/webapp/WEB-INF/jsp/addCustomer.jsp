<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AddCustomer</title>
</head>
<body>
    <div align="center">
        <h1>Input New Data Customer [Test Spring MVC]</h1>
    <f:form action="${pageContext.request.contextPath}/customer/save" modelAttribute="dto">
    <table>
            <tr>
                <td><label>Nama Customer : </label></td>
                <td><f:input path="namaCustomer" style="font-size:125%;"/></td>
                <td><f:errors path="namaCustomer"/></td>
            </tr>
            <tr>
                <td><label>Jenis Kelamin:</label></td> 
                <td>Pria <f:radiobutton path="jenisKelamin" value="PRIA"/>
                Wanita <f:radiobutton path="jenisKelamin" value="WANITA"/></td>
                <td><f:errors path="jenisKelamin"/></td>
            </tr>
            <tr>
                <td><label>Alamat : </label></td>
                <td><f:input path="alamatCustomer" style="font-size:125%;"/></td>
                <td><f:errors path="alamatCustomer"/></td>
            </tr>
            <tr>
                <td><label>Email : </label></td>
                <td><f:input path="emailCustomer" style="font-size:125%;"/></td>
                <td><f:errors path="emailCustomer"/></td>
            </tr>
            <tr>
                <td><label>Kota : </label></td>
                <td>
                    <f:select path="kodeKota" style="font-size:125%;">
                    <f:option value="" label="Pilih Kota"/>
                    <c:forEach var="k" items="${listKota}">
                        <f:option value="${k.kodeKota}"
                        label="${k.namaKota}"/>
                    </c:forEach>
                    </f:select>
                </td>
                <td><f:errors path="kodeKota"/></td>
            </tr>
            <tr>
                <td>
                <br>
                    <input type="submit" value="Save">
                    <input type="button" value="Back" 
                        onclick="location.href='${pageContext.request.contextPath}/customer/list';">
                </td>
            </tr>
       </table>
       </f:form>
    </div>
</body>
</html>