<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Header</title>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/style.css">

</head>
<body>
	<div align="center">
		<h1>List Order</h1>
	</div>
	
	<input type="button" value="Tambah"
		onclick="location.href='${pageContext.request.contextPath}/kota/tambah'">
	<br>
	<br>
	<table>
	  <tr>
	    <th>No Nota</th>
	    <th>Tanggal Transaksi</th>
	    <th>Harga Total</th>
	    <th>Global Diskon</th>
	    <th>Nama Customer</th>
	    <th>Nama Karyawan</th>
	    <th>Action</th>
	  </tr>
	  <c:forEach items="${listorder}" var = "o">
	  	<tr>
		    <td>${o.noNota}</td>
		    <td>${o.tanggalTransaksi}</td>
		    <td>${o.hargaTotal}</td>
		    <td>${o.globalDiskon}</td>
		    <td>${o.namaCustomer}</td>
		    <td>${o.namaKaryawan}</td>
		    <td>
		    	<input type="button" value="Details"
			    onclick="location.href='${pageContext.request.contextPath}/order/details/${o.noNota}';">
		    	<input type="button" value="Hapus"
				    onclick="location.href='${pageContext.request.contextPath}/order/hapus/${o.noNota}';">
		    </td>
		</tr>
	  </c:forEach>
	  
	</table>
</body>
</html>