<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="d"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ListBarang</title>
<style>
    table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 10px;
        }
        tr:nth-child(even) {
          background-color: #90EE90;
        }
        table, th, td {
            border:1.5px solid black;
        }
</style>
</head>
<body>
    <div align="center" style="font-size:100%;">
        <h1>DATA BARANG PENJUALAN SPRING MVC</h1>
    </div>
    <div align="center" style="font-size:100%;">
    <f:form action="${pageContext.request.contextPath}/product/find;">
    <input type="text" name="parameter" onfocus="this.value=''" 
                value="search by name..." style="width:300px;" />
            <input type="submit" value="search" style="width:100px;"
                onclick="location.href='${pageContext.request.contextPath}/product/list';"/>
     </f:form>
     <br>       
            <input type="submit" value="Main Menu" 
                style="height:40px;background-color:powderblue;width:200px;"
                onclick="location.href='${pageContext.request.contextPath}';"/>
            <input type="button" value="New Data" 
                style="height:40px;background-color:powderblue;width:200px;"
                onclick="location.href='${pageContext.request.contextPath}/product/add';"/>
            <input type="submit" value="All Data" 
                style="height:40px;background-color:powderblue;width:200px;"
                onclick="location.href='${pageContext.request.contextPath}/product/list';"/>
    </div>
    <br>
    <br>
    <table>
          <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Stok Barang</th>
            <th>Nama Supplier</th>
            <th>Action</th>
          </tr>
          <d:forEach items="${listBarang}" var="b">
          <tr style="font-size:105%;">
            <td>${b.kodeBarang}</td>
            <td>${b.namaBarang}</td>
            <td>${b.stokBarang}</td>
            <td>${b.namaSupplier}</td>
            <td><input type="button" value="Edit"
            onclick="location.href='${pageContext.request.contextPath}/product/update/${b.kodeBarang}';">
            <input type="button" value="Delete"
            onclick="location.href='${pageContext.request.contextPath}/product/delete/${b.kodeBarang}';">
            </td>
          </tr>       
          </d:forEach>
        </table>
</body>
</html>