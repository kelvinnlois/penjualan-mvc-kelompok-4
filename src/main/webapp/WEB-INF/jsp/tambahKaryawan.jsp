<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tambah Karyawan</title>
</head>
<body>
	<div align = "center">
		<h1>Tambah Karyawan</h1>
	</div>
	
	<f:form action="${pageContext.request.contextPath}/karyawan/save" modelAttribute="dto">
		<table>
			<tr>
				<td><label>Nama Karyawan</label></td>
				<td><f:input path="namaKaryawan"/></td>
				<td><f:errors path="namaKaryawan"/></td>
			</tr>
			<tr>
				<td><label>Username</label></td>
				<td><f:input path="username"/></td>
				<td><f:errors path="username"/></td>
			</tr>
			<tr>
				<td><label>Level</label></td>
				<td><f:input path="level"/></td>
				<td><f:errors path="level"/></td>
			</tr>
			<tr>
				<td><label>Level Karyawan</label></td>
				<td><f:input path="levelKaryawan"/></td>
				<td><f:errors path="levelKaryawan"/></td>
			</tr>
			<tr>
				<td><label>Password</label></td>
				<td><f:input path="password"/></td>
				<td><f:errors path="password"/></td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Simpan">
					<input type="button" value="Kembali" onclick = "location.href='${pageContext.request.contextPath}/karyawan/list'">
				</td>
			</tr>
		</table>
	</f:form>
	
</body>
</html>