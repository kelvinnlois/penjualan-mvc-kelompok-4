<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tambah kota</title>
</head>
<body>
	<div align="center">
		<h1>Tambah Kota</h1>	
	</div>
	
	<f:form action="${pageContext.request.contextPath}/kota/save" modelAttribute="dto">
		<table>
			<tr>
				<td><label>Nama Kota</label></td>
				<td><f:input path="namaKota"/></td>
				<td><f:errors path="namaKota"/></td>
			</tr>
			<tr>
				<td><label> Nama Provinsi </label></td>
				<td>
					<f:select path="kodeProvinsi">
						<f:option value="" label="Pilih Provinsi"></f:option>
						<c:forEach var="k" items="${listProvinsi}">
							<f:option value="${k.kodeProvinsi}" label="${k.namaProvinsi}"/>
						</c:forEach>
					</f:select>
				</td>
				<td><f:errors path="kodeProvinsi"/></td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Simpan">
					<input type="button" value="Kembali" onclick="location.href='${pageContext.request.contextPath}/kota/list'">
				</td>
			</tr>
		</table>
	</f:form>
</body>
</html>