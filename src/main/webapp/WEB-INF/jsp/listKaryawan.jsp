<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Karyawan</title>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/style.css">

</head>
<body>

	<div align="center">
		<h1>List Karyawan</h1>
	</div>
	<input type="button" value="Tambah"
		onclick="location.href='${pageContext.request.contextPath}/karyawan/tambah'">
	<br>
	<br>
	<table>
	  <tr>
	    <th>Kode</th>
	    <th>Nama</th>
	    <th>Level</th>
	    <th>Level Karyawan</th>
	    <th>Username</th>
	    <th>Action</th>
	  </tr>
	  <c:forEach items="${listkaryawan}" var = "k">
	  	<tr>
		    <td>${k.kodeKaryawan}</td>
		    <td>${k.namaKaryawan}</td>
		    <td>${k.level}</td>
		    <td>${k.levelKaryawan}</td>
		    <td>${k.username}</td>
		    <td>
			    	<input type="button" value="Edit"
				    onclick="location.href='${pageContext.request.contextPath}/karyawan/detail/${k.kodeKaryawan}';">
				    <input type="button" value="Hapus"
				    onclick="location.href='${pageContext.request.contextPath}/karyawan/hapus/${k.kodeKaryawan}';">
			    </td>
		</tr>
	  </c:forEach>
	  
	</table>

</body>
</html>