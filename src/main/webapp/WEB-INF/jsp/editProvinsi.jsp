<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Provinsi</title>
</head>
<body>

<div align="center">
		<h1>Edit Provinsi</h1>
	</div>
	
	<f:form modelAttribute="dto" action="${pageContext.request.contextPath}/provinsi/save">
		<table>
			<tr>
				<td><label> Kode Provinsi </label></td>
				<td><f:input path="kodeProvinsi" readonly="true"/></td>
				<td><f:errors path="kodeProvinsi"/></td>
			</tr>
			<tr>
				<td><label> Nama Provinsi </label></td>
				<td><f:input path="namaProvinsi"/></td>
				<td><f:errors path="namaProvinsi"/></td>
			</tr>
			
			<tr>
				<td>
					<input type="submit" value="Simpan">
					<input type="button" value="Kembali" onclick="location.href='${pageContext.request.contextPath}/provinsi/list'">
				</td>
			</tr>
			
		</table>
	</f:form>


</body>
</html>
