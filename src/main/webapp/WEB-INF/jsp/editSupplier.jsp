<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Edit Supplier</title>
	</head>
	
	<body>
		<div align="center">
			<h1>Edit Supplier</h1>
		</div>
		
		<f:form modelAttribute="dto" action="${pageContext.request.contextPath}/supplier/save">
			<table>
				<tr>
					<td><label> Kode Supplier </label></td>
					<td><f:input path="kodeSupplier" readonly="true"/></td>
				</tr>
				<tr>
					<td><label> Nama Supplier </label></td>
					<td><f:input path="namaSupplier"/></td>
					<td><f:errors path="namaSupplier"/></td>
				</tr>
				<tr>
					<td><label> Alamat </label></td>
					<td><f:input path="alamatSupplier"/></td>
					<td><f:errors path="alamatSupplier"/></td>
				</tr>
				<tr>
					<td><label> Telepon </label></td>
					<td><f:input path="telpSupplier"/></td>
					<td><f:errors path="telpSupplier"/></td>
				</tr>
				<tr>
					<td><label> Email </label></td>
					<td><f:input path="emailSupplier"/></td>
					<td><f:errors path="emailSupplier"/></td>
				</tr>
				<tr>
					<td><label> Kota </label></td>
					<td>
						<f:select path="kodeKota">
							<f:option value="" label="Pilih Kota"></f:option>
							<c:forEach var="k" items="${listKota}">
								<f:option value="${k.kodeKota}" label="${k.namaKota}"></f:option>
							</c:forEach>				
						</f:select>
					</td>
					<td><f:errors path="kodeKota"/></td>
				</tr> 
				
				<tr>
					<td>
						<input type="submit" value="Simpan">
						<input type="button" value="Kembali" onclick="location.href='${pageContext.request.contextPath}/supplier/list'">
					</td>
				</tr>
				
			</table>
		</f:form>

	</body>
</html>