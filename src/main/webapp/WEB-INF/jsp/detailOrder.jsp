<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Order</title>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/style.css">

</head>
<body>
	<div>
		<h1>Detail Order</h1>
	</div>
	<div>
	<c:forEach items="${listheader}" var = "h">
		<p>No Nota : ${h.noNota}</p>
		<p>Tanggal Transaksi : ${h.tanggalTransaksi}</p>
		<p>Customer : ${h.namaCustomer}</p>
		<p>Karyawan : ${h.namaKaryawan}</p>
	</c:forEach>
	</div>
	
	<table>
	  <tr>
	    <th>Kode Detail</th>
	    <th>Nama Barang</th>
	    <th>Quantity</th>
	    <th>Diskon</th>
	    <th>Subtotal</th>
	    <th>Action</th>
	  </tr>
	  <c:forEach items="${listdetail}" var = "d">
	  	<tr>
		    <td>${d.kodeDetail}</td>
		    <td>${d.namaBarang}</td>
		    <td>${d.qty}</td>
		    <td>${d.diskon}</td>
		    <td>${d.subtotal}</td>
		    <td>
		    	<input type="button" value="Edit"
			    onclick="location.href='${pageContext.request.contextPath}/order/details/${o.noNota}';">
		    </td>
		</tr>
	  </c:forEach>
	  
	</table>
	
</body>
</html>