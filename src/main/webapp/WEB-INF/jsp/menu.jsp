<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styleMenu.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" 
    name="viewport" content="width=device-width, initial-scale=1">
<title>MainMenu</title>
</head>
<body>

    <div id="mySidepanel" class="sidepanel" align="center">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">X Close</a>
        <a href="#">KELOMPOK 4</a>
        <a href="#">1. Kelvin</a>
        <a href="#">2. Ghifari</a>
        <a href="#">3. Geo</a>
    </div>
    
    <div align="left">
    <button class="openbtn" onclick="openNav()"> @AUTHOR</button>
    </div>
    <br>
    <div align="left">
    <input type="button" value="Log Out"
            onclick="location.href=''">
    </div>
    
    <div align="center">
        <h1>${message}</h1>    
    </div>
    <br>
    <h2>MENU</h2>
    <div class="tab">
        <button class="tablinks" onclick="location.href='${pageContext.request.contextPath}/product/list';" 
            onmouseover="openDesc(event, 'Products')">Products</button>
        <button class="tablinks" onclick="location.href='${pageContext.request.contextPath}/customer/list';" 
            onmouseover="openDesc(event, 'Customers')">Customers</button>
        <button class="tablinks" onclick=""
            onmouseover="openDesc(event, 'Employees')">Employees</button>
        <button class="tablinks" onclick="" 
            onmouseover="openDesc(event, 'Cities')">Cities</button>
        <button class="tablinks" onclick="" 
            onmouseover="openDesc(event, 'States')">States</button>
        <button class="tablinks" onclick="" 
            onmouseover="openDesc(event, 'Suppliers')">Suppliers</button>
        <button class="tablinks" onclick="" 
            onmouseover="openDesc(event, 'Orders')">Orders</button>
    </div>
    
    <div id="Products" class="tabcontent">
        <h3>Show All Data of Products</h3>
        <p>ID, Product Name, Quantity, and Supplier Name</p>
    </div>

    <div id="Customers" class="tabcontent">
        <h3>Show All Data of Customers</h3>
        <p>Customer ID and customer information</p> 
    </div>

    <div id="Employees" class="tabcontent">
        <h3>Show All Data of Employees</h3>
        <p>Description</p>
    </div>
    
    <div id="Cities" class="tabcontent">
        <h3>Show All Data of Cities</h3>
        <p>Description</p>
    </div>
    
    <div id="States" class="tabcontent">
        <h3>Show All Data of States</h3>
        <p>Description</p>
    </div>
    
    <div id="Suppliers" class="tabcontent">
        <h3>Show All Data of Suppliers</h3>
        <p>Description</p>
    </div>
    
    <div id="Orders" class="tabcontent">
        <h3>Show All Data of Orders</h3>
        <p>Description</p>
    </div>
    
    
    <div class="clearfix"></div>
    
    <script>
    function openDesc(evt, titleName) {
    	var i, tabcontent, tablinks;
    	  tabcontent = document.getElementsByClassName("tabcontent");
    	  for (i = 0; i < tabcontent.length; i++) {
    		    tabcontent[i].style.display = "none";
    	}
    	  tablinks = document.getElementsByClassName("tablinks");
    	  for (i = 0; i < tablinks.length; i++) {
    		    tablinks[i].className = tablinks[i].className.replace(" active", "");
    		    }
    	  document.getElementById(titleName).style.display = "block";
    	  evt.currentTarget.className += " active";
    	    }
    function openNav() {
    	  document.getElementById("mySidepanel").style.width = "1000px";
    	  document.getElementById("mySidepanel").style.height = "500px";
    	}

    	function closeNav() {
    	  document.getElementById("mySidepanel").style.width = "0";
    	}
    </script>
</body>
</html>