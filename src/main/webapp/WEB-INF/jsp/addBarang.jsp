<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AddProduct</title>
</head>
<body>
    <div align="center">
        <h1>Input New Data Product [Test Spring MVC]</h1>
    <f:form action="${pageContext.request.contextPath}/product/save" modelAttribute="dto">
    <table>
            <tr>
                <td><label>Nama Barang : </label></td>
                <td><f:input path="namaBarang" style="font-size:125%;"/></td>
                <td><f:errors path="namaBarang"/></td>
            </tr>
            <tr>
                <td><label>Stok Barang : </label></td>
                <td><f:input path="stokBarang" style="font-size:125%;"/></td>
                <td><f:errors path="stokBarang"/></td>
            </tr>
            <tr>
                <td><label>Nama Supplier : </label></td>
                <td>
                    <f:select path="kodeSupplier" style="font-size:125%;">
                    <f:option value="" label="Choose Supplier"/>
                    <c:forEach var="s" items="${listSupplier}">
                        <f:option value="${s.kodeSupplier}"
                        label="${s.namaSupplier}"/>
                    </c:forEach>
                    </f:select>
                </td>
                <td><f:errors path="kodeSupplier"/></td>
            </tr>
            <tr>
                <td>
                <br>
                    <input type="submit" value="Save">
                    <input type="button" value="Back" 
                        onclick="location.href='${pageContext.request.contextPath}/product/list';">
                </td>
            </tr>
       </table>
       </f:form>
    </div>
</body>
</html>