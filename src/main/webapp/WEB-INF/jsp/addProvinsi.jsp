<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Tambah Provinsi</title>
		
		<style>
			.error{color : red}
		</style>
		
	</head>

<body>

	<div align="center">
		<h1>Tambah Provinsi</h1>
	</div>
	
	<f:form modelAttribute="dto" action="${pageContext.request.contextPath}/provinsi/save">
		<table>
			<tr>
				<td><label> Nama Provinsi </label></td>
				<td><f:input path="namaProvinsi"/></td>
				<td><f:errors path="namaProvinsi" cssClass = "error"/></td>
			</tr> 
			
			<tr>
				<td>
					<input type="submit" value="Simpan">
					<input type="button" value="Kembali" onclick="location.href='${pageContext.request.contextPath}/provinsi/list'">
				</td>
			</tr>
			
		</table>
	</f:form>

</body>
</html>


