<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Halaman Kota</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
</head>
<body>
	
	<section class="ftco-section">
		<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6 text-center mb-5">
				<h2 class="heading-section">List Kota</h2>
			</div>
		</div>
		<input type="button" value="Tambah"
			onclick="location.href='${pageContext.request.contextPath}/kota/tambah'">
		<div class="row">
			<div class="col-md-12">
				<div class="table-wrap">
					<table class="table">
					    <thead class="thead-primary">
					    	<tr>
						  		<th>Kode</th>
						    	<th>Nama Kota</th>
						    	<th>Nama Provinsi</th>
						    	<th>Action</th>
					    	</tr>
				    	</thead>
				    	<c:forEach items="${listkota}" var = "k">
					  		<tr>
					  			<th scope="row" class="scope">${k.kodeKota}</th>
							    <td>${k.namaKota}</td>
							    <td>${k.namaProvinsi}</td>
							    <td style="text-align:center">
							    	<input type="button" value="Edit" class="btn btn-primary"
								    onclick="location.href='${pageContext.request.contextPath}/kota/detail/${k.kodeKota}';">
								    <input type="button" value="Hapus" class="btn btn-warning"
								    onclick="location.href='${pageContext.request.contextPath}/kota/hapus/${k.kodeKota}';">
							    </td>
							</tr>
					  	</c:forEach>
				    </table>
			    </div>
			</div>
		</div>  
		</div>
	</section>
	
	
		  
</body>
</html>