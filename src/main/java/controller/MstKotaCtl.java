package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstKotaDto;
import service.MstKotaSvc;
import service.MstProvinsiSvc;

@Controller
@RequestMapping(value="/kota")
public class MstKotaCtl {
	
	@Autowired
	MstKotaSvc svcKota;
	
	@Autowired
	MstProvinsiSvc svcProvinsi;
	
	@RequestMapping(value="/list")
	public String listKota(Model model,
			@RequestParam(value="cari", defaultValue = "",
			required = false) String namaKota){
		
		List<MstKotaDto> list = svcKota.findKota(namaKota);
		model.addAttribute("listkota", list);
		return "listKota";
	}
	
	@RequestMapping(value="/tambah")
	public String tambahKota(Model model){
		
		MstKotaDto dto = new MstKotaDto();
		model.addAttribute("listProvinsi", svcProvinsi.listProvinsi());
		model.addAttribute("dto", dto);
		
		return "tambahKota";
	}
	
	@RequestMapping(value="/save")
	public String simpan(@Valid @ModelAttribute("dto")
		MstKotaDto dto, BindingResult result, Model model){
		if(result.hasErrors()){
			model.addAttribute("listProvinsi", svcProvinsi.listProvinsi());
			return "tambahKota";
		} else {
			svcKota.save(dto);
			return "redirect:/kota/list";
		}
		
	}
	
	@RequestMapping(value="/edit")
	public String edit(@Valid @ModelAttribute("dto") MstKotaDto dto,
			BindingResult result, Model model){
		if(result.hasErrors()){
			model.addAttribute("listProvinsi", svcProvinsi.listProvinsi());
			return "updateKota";
		} else {
			svcKota.save(dto);
			return "redirect:/kota/list";
		}
		
	}
	
	@RequestMapping(value="/detail/{kode}")
	public String detail(Model model, @PathVariable("kode") String kodeKota){
		model.addAttribute("listProvinsi", svcProvinsi.listProvinsi());
		MstKotaDto dto = svcKota.findOne(kodeKota);
		model.addAttribute("dto", dto);
		
		return "updateKota";
	}
	
	@RequestMapping(value="hapus/{kode}")
	public String hapus(@PathVariable("kode")String kodeKota){
		
		svcKota.delete(kodeKota);
		return "redirect:/kota/list";
	}
}
