package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstCustomerDto;
import service.MstCustomerSvc;
import service.MstKotaSvc;

@Controller
@RequestMapping(value="customer")
public class MstCustomerCtl {
	
	@Autowired
	MstCustomerSvc svcCustomer;
	
	@Autowired
	MstKotaSvc svcKota;
	
	@RequestMapping(value="list")
	public String findDataCustomer(
			Model model,
			@RequestParam(value="search", defaultValue="", required=false)String kodeCustomer){
		List<MstCustomerDto> listCustomer = svcCustomer.findCustomer(kodeCustomer);
		model.addAttribute("listCustomer", listCustomer);
		return "listCustomer";
	}
	
	@RequestMapping(value="find")
	public String find(HttpServletRequest request){
		
		String parameter = request.getParameter("parameter");
		return "redirect:/customer/list?search="+parameter;
		
	}
	
	@RequestMapping(value="addCustomer")
	public String addDataCustomer(Model model){
		MstCustomerDto dto = new MstCustomerDto();
		model.addAttribute("dto", dto);
		model.addAttribute("listKota", svcKota.listKota());
		return "addCustomer";
	}
	
	@RequestMapping(value="save")
	public String save(@Valid @ModelAttribute("dto") MstCustomerDto dto,
			BindingResult result, Model model){
		if(result.hasErrors()){
			model.addAttribute("listKota", svcKota.listKota());
			if(dto.getKodeCustomer() == null){
				return "addCustomer";				
			}else{
				return "updateCustomer";
			}
		}else{
			svcCustomer.save(dto);			
			return "redirect:/customer/list";//mapping return
		}
	}
	
	@RequestMapping(value="update/{no}")
	public String detail(Model model, @PathVariable("no")String kodeCustomer){
		MstCustomerDto detailDto = svcCustomer.findOne(kodeCustomer);
		model.addAttribute("dto", detailDto);
		model.addAttribute("listKota", svcKota.listKota());
		return "updateCustomer";
	}
	
	@RequestMapping(value="delete/{no}")
	public String detail(@PathVariable("no")String kodeCustomer){
		svcCustomer.delete(kodeCustomer);
		return "redirect:/customer/list";
	}
}
