package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import dto.MstKotaDto;
import dto.MstSupplierDto;
import service.MstKotaSvc;
import service.MstSupplierSvc;


@Controller
@RequestMapping(value="supplier")
public class SupplierCtl {
	
	@Autowired
	MstSupplierSvc svc;
	
	@Autowired
	MstKotaSvc svcKota;
	
	
	@RequestMapping(value = "list")
	public String findSupplier(
			Model model, 
			@RequestParam(value="cari", defaultValue = "", required = false) String cari) {
		List<MstSupplierDto> listDto = svc.findSupplier(cari);
		model.addAttribute("list", listDto);
		// return nama file .jps yang dibuat di folder jsp (dalam WEB-INF)
		return "listSupplier";
	}
	
	// Search by Name
	@RequestMapping(value = "/findByName")
	public String findByName(HttpServletRequest request) {
		String parameter = request.getParameter("parameter");	
		return "redirect:/supplier/list?cari=" + parameter;
	}
	
	// Insert Data Supplier
	@RequestMapping(value = "/add")
	public String Tambah(Model model) {
		MstSupplierDto dto = new MstSupplierDto();
		List<MstKotaDto> listKota =  svcKota.listKota();
		model.addAttribute("dto", dto);
		model.addAttribute("listKota", listKota);
		return "addSupplier";
	}
	
	@RequestMapping(value="/save")
	public String simpan(@Valid @ModelAttribute("dto") MstSupplierDto dto, BindingResult result, Model model) {
		if(result.hasErrors()) {
			model.addAttribute("listKota", svcKota.listKota());
			if(dto.getKodeSupplier() == null) {
				return "addSupplier";
			} else {
				return "editSupplier";
			}
		} else {
			svc.save(dto);
			return "redirect:/supplier/list";
		}
	}
	
	
	// Edit
	@RequestMapping("/detail/{kodeSupplier}")
	public String detail(Model model, @PathVariable("kodeSupplier") String kodeSupplier) {
		MstSupplierDto detailDto = svc.findOne(kodeSupplier);
		model.addAttribute("dto", detailDto);
		model.addAttribute("listKota", svcKota.listKota());
		return "editSupplier";
	}

	// Delete
	@RequestMapping("/delete/{kodeSupplier}")
	public String delete(@PathVariable("kodeSupplier") String kodeSupplier) {
		svc.delete(kodeSupplier);
		return "redirect:/supplier/list";
	}

}
