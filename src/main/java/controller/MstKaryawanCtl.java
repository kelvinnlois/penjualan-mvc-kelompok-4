package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstKaryawanDto;
import service.MstKaryawanSvc;

@Controller
@RequestMapping(value="/karyawan")
public class MstKaryawanCtl {
	
	
	@Autowired
	MstKaryawanSvc svcKaryawan;
	
	@RequestMapping(value="/list")
	public String listKota(Model model,
			@RequestParam(value="cari", defaultValue = "",
			required = false) String namaKaryawan){
		
		List<MstKaryawanDto> list = svcKaryawan.findKaryawan(namaKaryawan);
		model.addAttribute("listkaryawan", list);
		return "listKaryawan";
	}
	
	@RequestMapping(value="/tambah")
	public String tambah(Model model){
		
		MstKaryawanDto dto = new MstKaryawanDto();
		
		model.addAttribute("dto", dto);
		
		return "tambahKaryawan";
	}
	
	@RequestMapping(value="/save")
	public String simpan(@Valid @ModelAttribute("dto")
			MstKaryawanDto dto, BindingResult result){
		if(result.hasErrors()){
			return "tambahKaryawan";
		} else {
			svcKaryawan.save(dto);
			return "redirect:/karyawan/list";
		}
	}
	
	@RequestMapping(value="detail/{kode}")
	public String detail(Model model, @PathVariable("kode") String kode){
		MstKaryawanDto dto = svcKaryawan.findKaryawanByKode(kode);
		model.addAttribute("dto", dto);
		
		return "updateKaryawan";
	}
	
	@RequestMapping(value="/edit")
	public String edit(@ModelAttribute("dto") MstKaryawanDto dto, Model model){
		
		svcKaryawan.save(dto);
		return "redirect:/karyawan/list";
	}
	
	
	@RequestMapping(value="hapus/{kode}")
	public String hapus(@PathVariable("kode")String kode){
		
		svcKaryawan.delete(kode);
		return "redirect:/karyawan/list";
	}
}
