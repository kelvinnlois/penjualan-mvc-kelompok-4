package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.TrDetailPenjualanDto;
import dto.TrHeaderPenjualanDto;
import service.TrDetailPenjualanSvc;
import service.TrHeaderPenjualanSvc;

@Controller
@RequestMapping(value="/order")
public class OrderCtl {
	
	@Autowired
	TrHeaderPenjualanSvc svcHeader;
	
	@Autowired
	TrDetailPenjualanSvc svcDetail;
	
	@RequestMapping(value="/list")
	public String list(Model model,
			@RequestParam(value="cari", defaultValue = "", required = false) String noNota){
		
		List<TrHeaderPenjualanDto> list = svcHeader.findOnePenjualan(noNota);
		model.addAttribute("listorder", list);
		return "listOrder";
	}
	
	@RequestMapping(value="/hapus/{noNota}")
	public String hapus(@PathVariable("noNota") String noNota){
		
		svcHeader.delete(noNota);
		svcDetail.delete(noNota);
		
		return "redirect:/order/list";
	}
	
	@RequestMapping(value="/details/{noNota}")
	public String details(Model model,
			@PathVariable("noNota")String noNota){
		List<TrHeaderPenjualanDto> listHeader = svcHeader.findOnePenjualan(noNota);
		List<TrDetailPenjualanDto> listDetail = svcDetail.findOne(noNota);
		
		model.addAttribute("listheader", listHeader);
		model.addAttribute("listdetail", listDetail);
		
		return "detailOrder";
	}
	
	@RequestMapping(value="/details/detail/{kodeDetail}")
	public String detailOrder(Model model, @PathVariable("kodeDetail") String kodeDetail){
		
		
		return null;
	}
}
