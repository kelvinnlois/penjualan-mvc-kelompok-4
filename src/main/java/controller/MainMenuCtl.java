package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="menu")
public class MainMenuCtl {
	
	@RequestMapping(value="main")
	public String testMvc(Model model){
		String title = "Penjualan MVC 4";
		model.addAttribute("message", title);
		return "menu";
	}
}
