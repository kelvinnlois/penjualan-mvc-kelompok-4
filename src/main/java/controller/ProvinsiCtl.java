package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstProvinsiDto;
import service.MstProvinsiSvc;

@Controller
@RequestMapping(value="/provinsi")
public class ProvinsiCtl {
	
	@Autowired
	MstProvinsiSvc svc;

	
	// Read List Provinsi
	@RequestMapping(value = "/list")
	public String findProvinsi(
			Model model, 
			@RequestParam(value="cari", defaultValue = "", required = false) String cari) {
		List<MstProvinsiDto> listDto = svc.findProvinsi(cari);
		model.addAttribute("list", listDto);
		// return nama file .jps yang dibuat di folder jsp (dalam WEB-INF)
		return "listProvinsi";
	}
	
	// Search by Name
	@RequestMapping(value = "/findByName")
	public String findByName(HttpServletRequest request) {
		String parameter = request.getParameter("parameter");	
		return "redirect:/provinsi/list?cari=" + parameter;
	}
	
	// Insert Data Provinsi
	@RequestMapping(value = "/add")
	public String Tambah(Model model) {
		MstProvinsiDto dto = new MstProvinsiDto();
		model.addAttribute("dto", dto);
		return "addProvinsi";
	}
	
	@RequestMapping(value="/save")
	public String simpan(@Valid @ModelAttribute("dto") MstProvinsiDto dto, BindingResult result, Model model) {
		if(result.hasErrors()) {
			if(dto.getKodeProvinsi() == null) {
				return "addProvinsi";
			} else {
//				return "redirect:/provinsi/detail/" + dto.getKodeProvinsi();
				return "editProvinsi";
			}
		} else {
			svc.save(dto);
			return "redirect:/provinsi/list";
		}
	}
	
	// Edit
	@RequestMapping("/detail/{kodeProvinsi}")
	public String detail(Model model, @PathVariable("kodeProvinsi") String kodeProvinsi) {
		MstProvinsiDto detailDto = svc.findOne(kodeProvinsi);
		model.addAttribute("dto", detailDto);
		return "editProvinsi";
	}
	
	// Delete
	@RequestMapping("/delete/{kodeProvinsi}")
	public String delete(@PathVariable("kodeProvinsi") String kodeProvinsi) {
		svc.delete(kodeProvinsi);
		return "redirect:/provinsi/list";
	}
	
	
	

}
