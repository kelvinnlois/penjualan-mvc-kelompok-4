package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstBarangDto;
import service.MstBarangSvc;
import service.MstSupplierSvc;

@Controller
@RequestMapping(value="product")
public class MstBarangCtl {
	
	@Autowired
	MstBarangSvc svcBarang;
	
	@Autowired
	MstSupplierSvc svcSupplier;
	
	@RequestMapping(value="list")
	public String findDataProduct(
			Model model,
			@RequestParam(value="search", defaultValue="", required=false)String namaBarang){
		List<MstBarangDto> listBarang = svcBarang.findProduct(namaBarang);
		model.addAttribute("listBarang", listBarang);
		return "listBarang";
	}
	
	@RequestMapping(value="find")
	public String find(HttpServletRequest request){
		String parameter = request.getParameter("parameter");
		return "redirect:/product/list?search="+parameter;
	}
	
	@RequestMapping(value="add")
	public String addDataProduct(Model model){
		MstBarangDto dto = new MstBarangDto();
		model.addAttribute("dto", dto);
		model.addAttribute("listSupplier", svcSupplier.listSupplier());
		return "addBarang";
	}
	
	@RequestMapping(value="save")
	public String save(@Valid @ModelAttribute("dto") MstBarangDto dto,
			BindingResult result, Model model){
		if(result.hasErrors()){
			model.addAttribute("listSupplier", svcSupplier.listSupplier());
			if(dto.getKodeBarang() == null){
				return "addBarang";				
			}else{
				return "updateBarang";
			}
		}else{
			svcBarang.save(dto);			
			return "redirect:/product/list";//mapping return
		}
	}
	
	@RequestMapping(value="update/{no}")
	public String detail(Model model, @PathVariable("no")String kodeBarang){
		MstBarangDto detailDto = svcBarang.findOne(kodeBarang);
		model.addAttribute("dto", detailDto);
		model.addAttribute("listSupplier", svcSupplier.listSupplier());
		return "updateBarang";
	}
	
	@RequestMapping(value="delete/{no}")
	public String detail(@PathVariable("no")String kodeBarang){
		svcBarang.delete(kodeBarang);
		return "redirect:/product/list";
	}
}