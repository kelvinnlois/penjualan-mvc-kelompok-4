package entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TR_DETAIL_PENJUALAN database table.
 * 
 */
@Embeddable
public class TrDetailPenjualanPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="KODE_DETAIL")
	private String kodeDetail;

	@Column(name="NO_NOTA")
	private String noNota;

	public TrDetailPenjualanPK() {
	}
	public String getKodeDetail() {
		return this.kodeDetail;
	}
	public void setKodeDetail(String kodeDetail) {
		this.kodeDetail = kodeDetail;
	}
	public String getNoNota() {
		return this.noNota;
	}
	public void setNoNota(String noNota) {
		this.noNota = noNota;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TrDetailPenjualanPK)) {
			return false;
		}
		TrDetailPenjualanPK castOther = (TrDetailPenjualanPK)other;
		return 
			this.kodeDetail.equals(castOther.kodeDetail)
			&& this.noNota.equals(castOther.noNota);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.kodeDetail.hashCode();
		hash = hash * prime + this.noNota.hashCode();
		
		return hash;
	}
}