package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the MST_USER database table.
 * 
 */
@Entity
@Table(name="MST_USER")
@NamedQuery(name="MstUser.findAll", query="SELECT m FROM MstUser m")
public class MstUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USER_ID")
	private long userId;

	@Column(name="ALAMAT")
	private String alamat;

	@Column(name="JENIS_KELAMIN")
	private String jenisKelamin;

	@Column(name="NAMA")
	private String nama;

	@Column(name="NO_TELPON")
	private String noTelpon;

	@Column(name="PASSWORD")
	private String password;

	@Column(name="TANGGAL_LAHIR")
	private Timestamp tanggalLahir;

	@Column(name="USERNAME")
	private String username;

	public MstUser() {
	}

	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoTelpon() {
		return this.noTelpon;
	}

	public void setNoTelpon(String noTelpon) {
		this.noTelpon = noTelpon;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getTanggalLahir() {
		return this.tanggalLahir;
	}

	public void setTanggalLahir(Timestamp tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}