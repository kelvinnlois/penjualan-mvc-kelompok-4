package service;

import java.util.List;

import dto.MstProvinsiDto;

public interface MstProvinsiSvc {

	List<MstProvinsiDto> findProvinsi(String cari); 
	List<MstProvinsiDto> listProvinsi();
	
	void save(MstProvinsiDto dto);
	void delete(String kodeProvinsi);
	MstProvinsiDto findOne(String kodeProvinsi);
}

