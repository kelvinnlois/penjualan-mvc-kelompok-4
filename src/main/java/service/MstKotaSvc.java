package service;

import java.util.List;

import dto.MstKotaDto;

public interface MstKotaSvc {
	
	
	List<MstKotaDto> findKota(String namaKota);
	List<MstKotaDto> listKota();
	MstKotaDto findOne(String kodeKota);
	
	void save(MstKotaDto dto);
	void delete(String kodeKota);
}
