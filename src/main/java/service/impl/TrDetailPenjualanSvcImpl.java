package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.TrDetailPenjualanDao;
import dto.TrDetailPenjualanDto;
import entity.TrDetailPenjualan;
import service.TrDetailPenjualanSvc;

@Service
@Transactional
public class TrDetailPenjualanSvcImpl implements TrDetailPenjualanSvc{
	
	@Autowired
	TrDetailPenjualanDao tdpDao;

	@Override
	public ArrayList<TrDetailPenjualanDto> findOne(String cari) {
		// TODO Auto-generated method stub
		ArrayList<TrDetailPenjualanDto> dtos = new ArrayList<>();
		List<Object[]> daos = tdpDao.findDetailPenjualan(cari);
		for(Object[] object : daos){
			TrDetailPenjualanDto dto = new TrDetailPenjualanDto();
			TrDetailPenjualan tdp = (TrDetailPenjualan) object[0];
			String namaBarang = (String) object[1];
			
			dto.setKodeDetail(tdp.getId().getKodeDetail());
			dto.setNamaBarang(namaBarang);
			dto.setDiskon(tdp.getDiskon());
			dto.setHargaSatuan(tdp.getHargaSatuan());
			dto.setQty(tdp.getQty());
			dto.setSubtotal(tdp.getSubtotal());
			
			dtos.add(dto);
		}
		return dtos;
	}

//	@Override
//	public void save(TrDetailPenjualanDto dto) {
//		// TODO Auto-generated method stub
//		TrDetailPenjualan trDetailPenjualan = new TrDetailPenjualan();
//		trDetailPenjualan.setDiskon(dto.getDiskon());
//		trDetailPenjualan.setHargaSatuan(dto.getHargaSatuan());
//		trDetailPenjualan.setId(dto.getId());
//		trDetailPenjualan.setKodeBarang(dto.getKodeBarang());
//		trDetailPenjualan.setQty(dto.getQty());
//		trDetailPenjualan.setSubtotal(dto.getSubtotal());
//	}

	@Override
	public void delete(String noNota) {
		// TODO Auto-generated method stub
		tdpDao.deleteByNoNota(noNota);
	}
	
	

}
