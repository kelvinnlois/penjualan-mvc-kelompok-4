package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstKotaDao;
import dto.MstKotaDto;
import entity.MstKota;
import service.MstKotaSvc;

@Service
@Transactional
public class MstKotaSvcImpl implements MstKotaSvc{
	
	@Autowired
	MstKotaDao dao;

	@Override
	public List<MstKotaDto> findKota(String namaKota) {
		List<MstKotaDto> dtos = new ArrayList<MstKotaDto>();
		List<Object[]> daos = dao.findKota(namaKota);
		for(Object[] object : daos){
			MstKotaDto dto = new MstKotaDto();
			MstKota kota =  (MstKota) object[0];
			String provinsi = (String) object[1];
			
			dto.setKodeKota(kota.getKodeKota());
			dto.setNamaKota(kota.getNamaKota());
			dto.setNamaProvinsi(provinsi);
			
			dtos.add(dto);
		}
		// TODO Auto-generated method stub
		return dtos;
	}

	@Override
	public List<MstKotaDto> listKota() {
		// TODO Auto-generated method stub
		List<MstKotaDto> dtos = new ArrayList<>();
		List<MstKota> daos = dao.findAllByOrderByNamaKotaAsc();
		for(MstKota kota: daos){
			MstKotaDto dto = new MstKotaDto();
			dto.setKodeKota(kota.getKodeKota());
			dto.setNamaKota(kota.getNamaKota());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public void save(MstKotaDto dto) {
		// TODO Auto-generated method stub
		MstKota kota = new MstKota();
		if(dto.getKodeKota() == null){
			String kode = dao.findLastKodeKota();
			kota.setKodeKota(setAutoCode(kode));
		} else {
			kota.setKodeKota(dto.getKodeKota());
		}
		kota.setKodeProvinsi(dto.getKodeProvinsi());
		kota.setNamaKota(dto.getNamaKota().trim());
		dao.save(kota);
	}

	@Override
	public void delete(String kodeKota) {
		// TODO Auto-generated method stub
		dao.delete(kodeKota);
	}

	@Override
	public MstKotaDto findOne(String kodeKota) {
		// TODO Auto-generated method stub
		MstKota kota = dao.findOne(kodeKota);
		MstKotaDto dto = new MstKotaDto();
		dto.setKodeKota(kota.getKodeKota());
		dto.setKodeProvinsi(kota.getKodeProvinsi());
		dto.setNamaKota(kota.getNamaKota());
		return dto;
	}
	
	private String setAutoCode(String lastKey){
		String code = lastKey.substring(0, 1);
		String nomorString = lastKey.substring(1,lastKey.length());
		int nomor = Integer.parseInt(nomorString) + 1;
		String nomorBaru = String.format("%d", nomor);
		if(nomorBaru.length() == 1){
			return String.format("%s00%s", code, nomorBaru);
		} else if (nomorBaru.length() == 2){
			return String.format("%s0%s", code, nomorBaru);
		} else {
			return String.format("%s%s", code, nomorBaru);
		}
	}
	
}
