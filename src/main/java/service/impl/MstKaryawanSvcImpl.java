package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstKaryawanDao;
import dto.MstKaryawanDto;
import entity.MstKaryawan;
import service.MstKaryawanSvc;

@Service
@Transactional
public class MstKaryawanSvcImpl implements MstKaryawanSvc{
	
	@Autowired
	MstKaryawanDao dao;

	@Override
	public List<MstKaryawanDto> findKaryawan(String namaKaryawan) {
		// TODO Auto-generated method stub
		List<MstKaryawanDto> dtos = new ArrayList<>();
		List<MstKaryawan> daos = dao.findKaryawan(namaKaryawan);
		for(MstKaryawan karyawan : daos){
			MstKaryawanDto dto = new MstKaryawanDto();
			dto.setKodeKaryawan(karyawan.getKodeKaryawan());
			dto.setLevel(karyawan.getLevel());
			dto.setLevelKaryawan(karyawan.getLevelKaryawan());
			dto.setNamaKaryawan(karyawan.getNamaKaryawan());
			dto.setUsername(karyawan.getUsername());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public void save(MstKaryawanDto dto) {
		// TODO Auto-generated method stub
		MstKaryawan karyawan = new MstKaryawan();
		if(dto.getKodeKaryawan() == null){
			String kode = setAutoCode(dao.findLastKodeKaryawan());
			karyawan.setKodeKaryawan(kode);
		} else {
			karyawan.setKodeKaryawan(dto.getKodeKaryawan());
		}
		karyawan.setLevel(dto.getLevel());
		karyawan.setLevelKaryawan(dto.getLevelKaryawan());
		karyawan.setNamaKaryawan(dto.getNamaKaryawan());
		karyawan.setUsername(dto.getUsername());
		karyawan.setPassword(dto.getPassword());
		dao.save(karyawan);
	}

	@Override
	public MstKaryawanDto findKaryawanByKode(String kodeKaryawan) {
		// TODO Auto-generated method stub
		MstKaryawanDto dto = new MstKaryawanDto();
		MstKaryawan karyawan = dao.findOne(kodeKaryawan);
		dto.setKodeKaryawan(karyawan.getKodeKaryawan());
		dto.setLevel(karyawan.getLevel());
		dto.setLevelKaryawan(karyawan.getLevelKaryawan());
		dto.setNamaKaryawan(karyawan.getNamaKaryawan());
		dto.setUsername(dto.getUsername());
		dto.setPassword(karyawan.getPassword());
		dto.setPassword(karyawan.getPassword());
		
		return dto;
	}

	@Override
	public void delete(String kodeKaryawan) {
		// TODO Auto-generated method stub
		dao.delete(kodeKaryawan);
	}
	
	private String setAutoCode(String lastKey){
		String code = lastKey.substring(0, 1);
		String nomorString = lastKey.substring(1,lastKey.length());
		int nomor = Integer.parseInt(nomorString) + 1;
		String nomorBaru = String.format("%d", nomor);
		if(nomorBaru.length() == 1){
			return String.format("%s00%s", code, nomorBaru);
		} else if (nomorBaru.length() == 2){
			return String.format("%s0%s", code, nomorBaru);
		} else {
			return String.format("%s%s", code, nomorBaru);
		}
	}

}
