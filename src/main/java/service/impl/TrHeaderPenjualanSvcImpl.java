package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.TrHeaderPenjualanDao;
import dto.TrHeaderPenjualanDto;
import entity.TrHeaderPenjualan;
import service.TrHeaderPenjualanSvc;

@Service
@Transactional
public class TrHeaderPenjualanSvcImpl implements TrHeaderPenjualanSvc{
	
	@Autowired
	TrHeaderPenjualanDao thpDao;
	
//	@Override
//	public List<TrHeaderPenjualanDto> findAllPenjualan() {
//		// TODO Auto-generated method stub
//		List<TrHeaderPenjualanDto> dtos = new ArrayList<>();
//		List<Object[]> daos = thpDao.findAllHeaderPenjualan();
//		for(Object[] object : daos){
//			TrHeaderPenjualanDto dto = new TrHeaderPenjualanDto();
//			
//			TrHeaderPenjualan trHeaderPenjualan = (TrHeaderPenjualan) object[0];
//			String namaCustomer = object[1].toString();
//			String namaKaryawan = object[2].toString();
//			
//			dto.setGlobalDiskon(trHeaderPenjualan.getGlobalDiskon());
//			dto.setNamaCustomer(namaCustomer);
//			dto.setNamaKaryawan(namaKaryawan);
//			dto.setNoNota(trHeaderPenjualan.getNoNota());
//			dto.setTanggalTransaksi(trHeaderPenjualan.getTanggalTransaksi());
//			
//			dtos.add(dto);
//		}
//		return dtos;
//	}
	
	@Override
	public List<TrHeaderPenjualanDto> findOnePenjualan(String noNota) {
		// TODO Auto-generated method stub
		List<Object[]> daos = thpDao.findOneHeaderPenjualan(noNota);
		List<TrHeaderPenjualanDto> dtos = new ArrayList<>();
		for(Object[] object : daos){
			TrHeaderPenjualanDto dto = new TrHeaderPenjualanDto();
			TrHeaderPenjualan thp = (TrHeaderPenjualan) object[0];
			String namaCustomer = object[1].toString();
			String namaKaryawan = object[2].toString();
			
			dto.setGlobalDiskon(thp.getGlobalDiskon());
			dto.setNamaCustomer(namaCustomer);
			dto.setNamaKaryawan(namaKaryawan);
			dto.setNoNota(thp.getNoNota());
			dto.setTanggalTransaksi(thp.getTanggalTransaksi());
			dto.setHargaTotal(thp.getHargaTotal());
			dtos.add(dto);
		}
		return dtos;
	}
	
	@Override
	public void save(TrHeaderPenjualanDto dto) {
		// TODO Auto-generated method stub
		TrHeaderPenjualan thp = new TrHeaderPenjualan();
		thp.setGlobalDiskon(dto.getGlobalDiskon());
		thp.setHargaTotal(dto.getHargaTotal());
		thp.setKodeCustomer(dto.getKodeCustomer());
		thp.setKodeKaryawan(dto.getKodeKaryawan());
		thp.setNoNota(dto.getNoNota());
		thp.setTanggalTransaksi(dto.getTanggalTransaksi());
		thpDao.save(thp);
	}

	@Override
	public void delete(String noNota) {
		// TODO Auto-generated method stub
		thpDao.deleteByNoNota(noNota);
		
	}



}
