package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstProvinsiDao;
import dto.MstProvinsiDto;
import entity.MstProvinsi;
import service.MstProvinsiSvc;

@Service
@Transactional
public class MstProvinsiSvcImpl implements MstProvinsiSvc {
	
	@Autowired
	MstProvinsiDao dao;

	@Override
	public List<MstProvinsiDto> findProvinsi(String cari) {
		List<MstProvinsiDto> list = new ArrayList<>();
		List<Object[]> listObj = dao.findOneProvinsi(cari);
		for(Object[] objArr : listObj) {
			MstProvinsiDto dto = new MstProvinsiDto();
			dto.setKodeProvinsi((String) objArr[0]);
			dto.setNamaProvinsi((String) objArr[1]);
			list.add(dto);
		}
		return list;
	}

	@Override
	public void save(MstProvinsiDto dto) {
		MstProvinsi newProvinsi = new MstProvinsi();
		String lastKode = dao.findLastKodeProvinsi();
		String newKode = setAutoCode(lastKode);
		if(dto.getKodeProvinsi() == null) {
			newProvinsi.setKodeProvinsi(newKode);
		} else {
			newProvinsi.setKodeProvinsi(dto.getKodeProvinsi());
		}
		newProvinsi.setNamaProvinsi(dto.getNamaProvinsi());
		dao.save(newProvinsi);
	}

	
	
	@Override
	public void delete(String kodeProvinsi) {
		dao.delete(kodeProvinsi);
	}

	@Override
	public MstProvinsiDto findOne(String kodeProvinsi) {
		MstProvinsiDto dto = new MstProvinsiDto();
		MstProvinsi provinsi =  dao.findOne(kodeProvinsi);
		if(provinsi != null) {
			dto.setKodeProvinsi(provinsi.getKodeProvinsi());
			dto.setNamaProvinsi(provinsi.getNamaProvinsi());
			return dto;
		}
		return null;
	}
	
	// Helper Methods
	private String setAutoCode(String lastKey){
		String code = lastKey.substring(0, 1);
		String nomorString = lastKey.substring(1,lastKey.length());
		int nomor = Integer.parseInt(nomorString) + 1;
		String nomorBaru = String.format("%d", nomor);
		if(nomorBaru.length() == 1){
			return String.format("%s00%s", code, nomorBaru);
		} else if (nomorBaru.length() == 2){
			return String.format("%s0%s", code, nomorBaru);
		} else {
			return String.format("%s%s", code, nomorBaru);
		}
	}

	@Override
	public List<MstProvinsiDto> listProvinsi() {
		List<MstProvinsiDto> dtos = new ArrayList<>();
		List<MstProvinsi> listProvinsi = dao.findAllByOrderByNamaProvinsiAsc();
		for(MstProvinsi provinsi : listProvinsi) {
			MstProvinsiDto dto = new MstProvinsiDto();
			dto.setKodeProvinsi(provinsi.getKodeProvinsi());
			dto.setNamaProvinsi(provinsi.getNamaProvinsi());
			dtos.add(dto);
		}
		return dtos;
	}

}
