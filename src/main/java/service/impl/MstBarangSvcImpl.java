package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstBarangDao;
import dto.MstBarangDto;
import entity.MstBarang;
import service.MstBarangSvc;

@Transactional
@Service
public class MstBarangSvcImpl implements MstBarangSvc {
	
	@Autowired
	MstBarangDao daoBarang;

	@Override
	public List<MstBarangDto> findProduct(String namaBarang) {
		List<MstBarangDto> listBarang = new ArrayList<>();
		List<Object[]> list = daoBarang.findProduct(namaBarang);
		for(Object[] o : list){
			MstBarang b = (MstBarang) o[0];
			MstBarangDto dto = new MstBarangDto();
			dto.setKodeBarang(b.getKodeBarang());
			dto.setNamaBarang(b.getNamaBarang());
			dto.setStokBarang(b.getStokBarang());
			dto.setNamaSupplier((String) o[1]);
			listBarang.add(dto);
		}
		return listBarang;
	}

	@Override
	public void save(MstBarangDto dto) {
		// TODO Auto-generated method stub
		MstBarang b = new MstBarang();
		String lastCode = daoBarang.findLastKodeBarang();
		String newCode = setAutoCode(lastCode);
		b.setKodeBarang(setAutoCode(lastCode));
		
		if(dto.getKodeBarang() == null){
			b.setKodeBarang(newCode);
		}else{
			b.setKodeBarang(dto.getKodeBarang());
		}
		
		b.setNamaBarang(dto.getNamaBarang());
		b.setStokBarang(dto.getStokBarang());
		b.setKodeSupplier(dto.getKodeSupplier());
		daoBarang.save(b);
	}

	@Override
	public void delete(String kodeBarang) {
		// TODO Auto-generated method stub
		daoBarang.delete(kodeBarang);
	}

	@Override
	public MstBarangDto findOne(String kodeBarang) {
		// TODO Auto-generated method stub
		MstBarang b = daoBarang.findOne(kodeBarang);
		if( b != null ){
			MstBarangDto dto = new MstBarangDto();
			dto.setKodeBarang(b.getKodeBarang());
			dto.setNamaBarang(b.getNamaBarang());
			dto.setStokBarang(b.getStokBarang());
			dto.setKodeSupplier(b.getKodeSupplier());
			return dto; 
		}
		return null;
	}
	
	private String setAutoCode(String lastKey){
		String code = lastKey.substring(0, 1);
		String nomorString = lastKey.substring(1,lastKey.length());
		int nomor = Integer.parseInt(nomorString) + 1;
		String nomorBaru = String.format("%d", nomor);
		if(nomorBaru.length() == 1){
			return String.format("%s00%s", code, nomorBaru);
		} else if (nomorBaru.length() == 2){
			return String.format("%s0%s", code, nomorBaru);
		} else {
			return String.format("%s%s", code, nomorBaru);
		}
	}
}