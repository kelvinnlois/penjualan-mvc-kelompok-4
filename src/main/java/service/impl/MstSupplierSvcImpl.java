package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstKotaDao;
import dao.MstSupplierDao;
import dto.MstSupplierDto;
import entity.MstKota;
import entity.MstSupplier;
import service.MstSupplierSvc;


@Service
@Transactional
public class MstSupplierSvcImpl implements MstSupplierSvc {

	@Autowired
	MstSupplierDao dao;
	
	@Autowired
	MstKotaDao daoKota;
	
	@Override
	public List<MstSupplierDto> findSupplier(String cari) {
		List<MstSupplierDto> list = new ArrayList<>();
		List<Object[]> listObj = dao.findOneSupplier(cari);
		for(Object[] objArr : listObj) {
			MstSupplierDto dto = new MstSupplierDto();
			dto.setKodeSupplier((String) objArr[0]);
			dto.setNamaSupplier((String) objArr[1]);
			dto.setAlamatSupplier((String) objArr[2]);
			dto.setTelpSupplier((String) objArr[3]);
			dto.setEmailSupplier((String) objArr[4]);
			dto.setNamaKota((String) objArr[5]);
			list.add(dto);
		}
		return list;
	}

	@Override
	public void save(MstSupplierDto dto) {
		MstSupplier newSupplier = new MstSupplier();
		String lastKode = dao.findLastKodeSupplier();
		String newKode = setAutoCode(lastKode);
		if(dto.getKodeSupplier() == null) {
			newSupplier.setKodeSupplier(newKode);
		} else {
			newSupplier.setKodeSupplier(dto.getKodeSupplier());
		}
		newSupplier.setNamaSupplier(dto.getNamaSupplier());
		newSupplier.setAlamatSupplier(dto.getAlamatSupplier());
		newSupplier.setTelpSupplier(dto.getTelpSupplier());
		newSupplier.setEmailSupplier(dto.getEmailSupplier());
		newSupplier.setKodeKota(dto.getKodeKota());
		dao.save(newSupplier);
	}

	@Override
	public void delete(String kodeSupplier) {
		dao.delete(kodeSupplier);
	}

	@Override
	public MstSupplierDto findOne(String kodeSupplier) {
		MstSupplierDto dto = new MstSupplierDto();
		MstSupplier supplier = dao.findOne(kodeSupplier);
		if(supplier != null) {
			dto.setKodeSupplier(supplier.getKodeSupplier());
			dto.setNamaSupplier(supplier.getNamaSupplier());
			dto.setAlamatSupplier(supplier.getAlamatSupplier());
			dto.setEmailSupplier(supplier.getEmailSupplier());
			dto.setTelpSupplier(supplier.getTelpSupplier());
			
			String kodeKota = supplier.getKodeKota();
			MstKota kota = daoKota.findOne(kodeKota);
			
			dto.setKodeKota(kodeKota);
			dto.setNamaKota(kota.getNamaKota());
			return dto;
		}
		return null;
	}
	
	// Helper Methods
	private String setAutoCode(String lastKey){
		String code = lastKey.substring(0, 1);
		String nomorString = lastKey.substring(1,lastKey.length());
		int nomor = Integer.parseInt(nomorString) + 1;
		String nomorBaru = String.format("%d", nomor);
		if(nomorBaru.length() == 1){
			return String.format("%s00%s", code, nomorBaru);
		} else if (nomorBaru.length() == 2){
			return String.format("%s0%s", code, nomorBaru);
		} else {
			return String.format("%s%s", code, nomorBaru);
		}
	}

	@Override
	public List<MstSupplierDto> listSupplier() {
		List<MstSupplierDto> dtos = new ArrayList<>();
		List<MstSupplier> suppliers = dao.findAll();
		for(MstSupplier supplier : suppliers) {
			MstSupplierDto dto = new MstSupplierDto();
			dto.setKodeSupplier(supplier.getKodeSupplier());
			dto.setNamaSupplier(supplier.getNamaSupplier());
			dto.setAlamatSupplier(supplier.getAlamatSupplier());
			dto.setEmailSupplier(supplier.getEmailSupplier());
			dto.setTelpSupplier(supplier.getTelpSupplier());
			dto.setKodeKota(supplier.getKodeKota());
			dtos.add(dto);
		}
		return dtos;
	}

}
