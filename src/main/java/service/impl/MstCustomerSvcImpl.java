package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstCustomerDao;
import dto.MstCustomerDto;
import entity.MstCustomer;
import service.MstCustomerSvc;

@Transactional
@Service
public class MstCustomerSvcImpl implements MstCustomerSvc {
	
	@Autowired
	MstCustomerDao daoCustomer;

	@Override
	public List<MstCustomerDto> findCustomer(String kodeCustomer) {
		
		List<MstCustomerDto> listCustomer = new ArrayList<>();
		List<Object[]> list = daoCustomer.findCustomer(kodeCustomer);
		for(Object[] o : list){
			MstCustomer b = (MstCustomer) o[0];
			MstCustomerDto dto = new MstCustomerDto();
			dto.setKodeCustomer(b.getKodeCustomer());
			dto.setNamaCustomer(b.getNamaCustomer());
			dto.setAlamatCustomer(b.getAlamatCustomer());
			dto.setJenisKelamin(b.getJenisKelamin());
			dto.setEmailCustomer(b.getEmailCustomer());
			dto.setNamaKota((String) o[1]);
			listCustomer.add(dto);
		}
		return listCustomer;
		
	}

	@Override
	public void save(MstCustomerDto dto) {
		
		MstCustomer c = new MstCustomer();
		String lastCode = daoCustomer.findLastKodeCustomer();
		String newCode = setAutoCode(lastCode);
		c.setKodeCustomer(setAutoCode(lastCode));
		
		if(dto.getKodeCustomer() == null){
			c.setKodeCustomer(newCode);
		}else{
			c.setKodeCustomer(dto.getKodeCustomer());
		}
		
		c.setNamaCustomer(dto.getNamaCustomer());
		c.setJenisKelamin(dto.getJenisKelamin());
		c.setAlamatCustomer(dto.getAlamatCustomer());
		c.setEmailCustomer(dto.getEmailCustomer());
		c.setKodeKota(dto.getKodeKota());
		daoCustomer.save(c);
		
	}

	@Override
	public void delete(String kodeCustomer) {
		
		daoCustomer.delete(kodeCustomer);
		
	}

	@Override
	public MstCustomerDto findOne(String kodeCustomer) {
		
		MstCustomer c = daoCustomer.findOne(kodeCustomer);
		if( c != null){
			MstCustomerDto dto = new MstCustomerDto();
			dto.setKodeCustomer(c.getKodeCustomer());
			dto.setNamaCustomer(c.getNamaCustomer());
			dto.setJenisKelamin(c.getJenisKelamin());
			dto.setAlamatCustomer(c.getAlamatCustomer());
			dto.setEmailCustomer(c.getEmailCustomer());
			dto.setKodeKota(c.getKodeKota());
			return dto;
			
		}
		return null;
		
	}
	
	private String setAutoCode(String lastKey){
		
		String code = lastKey.substring(0, 1);
		String nomorString = lastKey.substring(1,lastKey.length());
		int nomor = Integer.parseInt(nomorString) + 1;
		String nomorBaru = String.format("%d", nomor);
		if(nomorBaru.length() == 1){
			return String.format("%s00%s", code, nomorBaru);
		} else if (nomorBaru.length() == 2){
			return String.format("%s0%s", code, nomorBaru);
		} else {
			return String.format("%s%s", code, nomorBaru);
		}
		
	}
}