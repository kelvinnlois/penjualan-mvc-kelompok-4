package service;

import java.util.List;

import dto.MstBarangDto;

public interface MstBarangSvc {
	
	List<MstBarangDto> findProduct(String namaBarang);
	
	public void save(MstBarangDto dto);
	public void delete(String kodeBarang);
	public MstBarangDto findOne(String kodeBarang);
	
}
