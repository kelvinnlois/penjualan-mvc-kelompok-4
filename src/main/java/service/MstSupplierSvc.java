package service;

import java.util.List;

import dto.MstSupplierDto;

public interface MstSupplierSvc {

	List<MstSupplierDto> findSupplier(String cari); 
	List<MstSupplierDto> listSupplier();
	
	void save(MstSupplierDto dto);
	void delete(String kodeSupplier);
	MstSupplierDto findOne(String kodeSupplier);
	
}
