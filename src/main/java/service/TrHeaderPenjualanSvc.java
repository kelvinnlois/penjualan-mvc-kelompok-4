package service;

import java.util.List;

import dto.TrHeaderPenjualanDto;

public interface TrHeaderPenjualanSvc {
	
	//List<TrHeaderPenjualanDto> findAllPenjualan();
	List<TrHeaderPenjualanDto> findOnePenjualan(String noNota);
	
	void save(TrHeaderPenjualanDto dto);
	void delete(String noNota);
}
