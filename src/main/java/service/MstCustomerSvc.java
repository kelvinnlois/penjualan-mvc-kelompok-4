package service;

import java.util.List;

import dto.MstCustomerDto;

public interface MstCustomerSvc {
	
	List<MstCustomerDto> findCustomer(String kodeCustomer);
	
	public void save(MstCustomerDto dto);
	public void delete(String kodeCustomer);
	public MstCustomerDto findOne(String kodeCustomer);
}
