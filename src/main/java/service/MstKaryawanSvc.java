package service;

import java.util.List;

import dto.MstKaryawanDto;

public interface MstKaryawanSvc {
	
	List<MstKaryawanDto> findKaryawan(String namakaryawan);
	MstKaryawanDto findKaryawanByKode(String kodeKaryawan);
	
	void save(MstKaryawanDto dto);
	void delete(String kodeKaryawan);
}
