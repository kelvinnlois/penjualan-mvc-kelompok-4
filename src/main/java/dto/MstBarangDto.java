package dto;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class MstBarangDto {
	
	private String kodeBarang;
	@NotEmpty(message="Product name is required !")
	private String namaBarang;
	@Min(value=0)
	private Integer stokBarang;
	private String namaSupplier;
	@NotEmpty(message="Supplier is required !")
	private String kodeSupplier;
	
	public String getKodeSupplier() {
		return kodeSupplier;
	}
	public void setKodeSupplier(String kodeSupplier) {
		this.kodeSupplier = kodeSupplier;
	}
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public String getNamaSupplier() {
		return namaSupplier;
	}
	public void setNamaSupplier(String kodeSupplier) {
		this.namaSupplier = kodeSupplier;
	}
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	public int getStokBarang() {
		return stokBarang;
	}
	public void setStokBarang(int stokBarang) {
		this.stokBarang = stokBarang;
	}
}