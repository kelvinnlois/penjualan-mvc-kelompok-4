package dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class MstCustomerDto {
	
	private String kodeCustomer;
	@NotEmpty(message="Customer name is required !")
	private String namaCustomer;
	@NotNull(message="Gender is required and choose only one !")
	private String jenisKelamin;
	@NotEmpty(message="Address is required !")
	private String alamatCustomer;
	
	@NotEmpty(message="Email is required !")
	@Email(message="Invalid email, please try again")
	private String emailCustomer;
	private String namaKota;
	@NotEmpty(message="City is required !")
	private String kodeKota;
	
	public String getKodeKota() {
		return kodeKota;
	}
	public void setKodeKota(String kodeKota) {
		this.kodeKota = kodeKota;
	}
	public String getKodeCustomer() {
		return kodeCustomer;
	}
	public void setKodeCustomer(String kodeCustomer) {
		this.kodeCustomer = kodeCustomer;
	}
	public String getAlamatCustomer() {
		return alamatCustomer;
	}
	public void setAlamatCustomer(String alamatCustomer) {
		this.alamatCustomer = alamatCustomer;
	}
	public String getEmailCustomer() {
		return emailCustomer;
	}
	public void setEmailCustomer(String emailCustomer) {
		this.emailCustomer = emailCustomer;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getNamaKota() {
		return namaKota;
	}
	public void setNamaKota(String namaKota) {
		this.namaKota = namaKota;
	}
	public String getNamaCustomer() {
		return namaCustomer;
	}
	public void setNamaCustomer(String namaCustomer) {
		this.namaCustomer = namaCustomer;
	}
	
	
	
}
