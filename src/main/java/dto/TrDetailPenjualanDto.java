package dto;

public class TrDetailPenjualanDto {
	
	private String kodeDetail;
	private int diskon;
	private int hargaSatuan;
	private int qty;
	private double subtotal;

	//MstBarang
	private String namaBarang;
	
	public String getKodeDetail() {
		return kodeDetail;
	}

	public void setKodeDetail(String kodeDetail) {
		this.kodeDetail = kodeDetail;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public int getDiskon() {
		return diskon;
	}

	public void setDiskon(int diskon) {
		this.diskon = diskon;
	}

	public int getHargaSatuan() {
		return hargaSatuan;
	}

	public void setHargaSatuan(int hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subTotal) {
		this.subtotal = subTotal;
	}
	
//	private double calculateSubTotal(){
//		double total = this.hargaSatuan * this.qty;
//		double discount = (100-this.diskon)*0.01;
//		return total * discount;
//	}
	
	
	
	
	
	
	
	
	
	
}
