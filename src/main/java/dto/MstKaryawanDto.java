package dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class MstKaryawanDto {
	
	private String kodeKaryawan;
	@NotNull
	private int level;
	@NotEmpty
	private String levelKaryawan;
	@NotEmpty
	private String namaKaryawan;
	@NotEmpty
	private String username;
	@NotEmpty
	@Size(min=8, max=20)
	private String password;
	
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getLevelKaryawan() {
		return levelKaryawan;
	}
	public void setLevelKaryawan(String levelKaryawan) {
		this.levelKaryawan = levelKaryawan;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	

}
