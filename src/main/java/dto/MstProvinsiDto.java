package dto;

import org.hibernate.validator.constraints.NotEmpty;

import validation.NotBlank;

public class MstProvinsiDto {
	
	private String kodeProvinsi;
	
	// try my custom validation annotation
	@NotBlank
	@NotEmpty(message = "Tidak Boleh Kosong")
	private String namaProvinsi;
	
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	public String getNamaProvinsi() {
		return namaProvinsi;
	}
	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
	
	

}
