package dto;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import validation.NotBlank;
import validation.ValidTelp;

public class MstSupplierDto {
	
	private String kodeSupplier;
	
	private String alamatSupplier;
	
//	@NotEmpty(message = "Tidak Boleh Kosong")
	@Pattern(regexp = "(?i)^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Format Email Tidak Valid")
	private String emailSupplier;
	
	@NotEmpty(message = "Tidak Boleh Kosong")
	private String kodeKota;
	
	// try my custom validation annotation
	@NotBlank
	@NotEmpty(message = "Tidak Boleh Kosong")
	private String namaSupplier;
	
	@ValidTelp(value = {"+628" , "0"}, message = "Format tidak valid : format +628xxxxxxxxxx, atau 08xxxxxxxxxx")
	private String telpSupplier;

	private String namaKota;
	
	
	public String getNamaKota() {
		return namaKota;
	}
	public void setNamaKota(String namaKota) {
		this.namaKota = namaKota;
	}
	public String getKodeSupplier() {
		return kodeSupplier;
	}
	public void setKodeSupplier(String kodeSupplier) {
		this.kodeSupplier = kodeSupplier;
	}
	public String getAlamatSupplier() {
		return alamatSupplier;
	}
	public void setAlamatSupplier(String alamatSupplier) {
		this.alamatSupplier = alamatSupplier;
	}
	public String getEmailSupplier() {
		return emailSupplier;
	}
	public void setEmailSupplier(String emailSupplier) {
		this.emailSupplier = emailSupplier;
	}
	public String getKodeKota() {
		return kodeKota;
	}
	public void setKodeKota(String kodeKota) {
		this.kodeKota = kodeKota;
	}
	public String getNamaSupplier() {
		return namaSupplier;
	}
	public void setNamaSupplier(String namaSupplier) {
		this.namaSupplier = namaSupplier;
	}
	public String getTelpSupplier() {
		return telpSupplier;
	}
	public void setTelpSupplier(String telpSupplier) {
		this.telpSupplier = telpSupplier;
	}
	
	

}
