package dto;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.springframework.format.annotation.DateTimeFormat;

public class TrHeaderPenjualanDto {
	
	private String noNota;
	private int globalDiskon;
	private double hargaTotal;
	private String kodeCustomer;
	private String kodeKaryawan;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Timestamp tanggalTransaksi;

	private String namaCustomer;
	private String namaKaryawan;
	
	private ArrayList<TrDetailPenjualanDto> details = new ArrayList<>();
	
	
	public ArrayList<TrDetailPenjualanDto> getDetails() {
		return details;
	}
	public void setDetails(ArrayList<TrDetailPenjualanDto> details) {
		this.details = details;
	}
	public String getNoNota() {
		return noNota;
	}
	public void setNoNota(String noNota) {
		this.noNota = noNota;
	}
	public int getGlobalDiskon() {
		return globalDiskon;
	}
	public void setGlobalDiskon(int globalDiskon) {
		this.globalDiskon = globalDiskon;
	}
	public double getHargaTotal() {
		return hargaTotal;
	}
	
	public void setHargaTotal(double hargaTotal) {
		this.hargaTotal = hargaTotal;
	}
	public String getKodeCustomer() {
		return kodeCustomer;
	}
	public void setKodeCustomer(String kodeCustomer) {
		this.kodeCustomer = kodeCustomer;
	}
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	public Timestamp getTanggalTransaksi() {
		return tanggalTransaksi;
	}
	public void setTanggalTransaksi(Timestamp tanggalTransaksi) {
		this.tanggalTransaksi = tanggalTransaksi;
	}
	public String getNamaCustomer() {
		return namaCustomer;
	}
	public void setNamaCustomer(String namaCustomer) {
		this.namaCustomer = namaCustomer;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	
//	private double calculateHargaTotal(){
//		double hargaTotal = 0;
//		for(TrDetailPenjualanDto tdpDto : details){
//			hargaTotal = hargaTotal + tdpDto.getSubtotal();
//		}
//		hargaTotal = hargaTotal * ((100-this.globalDiskon)*0.01);
//		return hargaTotal;
//	}
	
	

}
