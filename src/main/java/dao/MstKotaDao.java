package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstKota;

public interface MstKotaDao extends JpaRepository<MstKota, String>{
	
	@Query("select k, p.namaProvinsi from MstKota k, MstProvinsi p "
			+ "where k.kodeProvinsi = p.kodeProvinsi and k.namaKota like %:namaKota%")
	List<Object[]> findKota(@Param("namaKota")String namaKota);
	
	@Query(value="select top 1 k.KODE_KOTA from MST_KOTA as k order by k.KODE_KOTA desc", nativeQuery= true)
	String findLastKodeKota();
	
	List<MstKota> findAllByOrderByNamaKotaAsc();
	
}
