package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstKaryawan;

public interface MstKaryawanDao extends JpaRepository<MstKaryawan, String>{
	
	@Query("select k from MstKaryawan k "
			+ "where k.namaKaryawan like %:namaKaryawan%")
	List<MstKaryawan> findKaryawan(@Param("namaKaryawan")String namaKaryawan);
	
	@Query(value="select top 1 k.KODE_KARYAWAN from MST_KARYAWAN as k order by k.KODE_KARYAWAN desc", nativeQuery= true)
	String findLastKodeKaryawan();

	MstKaryawan findByUsername(String username);
}
