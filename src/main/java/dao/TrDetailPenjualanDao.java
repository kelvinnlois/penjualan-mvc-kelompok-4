package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.TrDetailPenjualan;
import entity.TrDetailPenjualanPK;

public interface TrDetailPenjualanDao extends JpaRepository<TrDetailPenjualan, TrDetailPenjualanPK>{
	
	@Query("select distinct tdp, mb.namaBarang from "
			+ "TrDetailPenjualan tdp, MstCustomer cus, MstKaryawan kar, MstBarang mb "
			+ "where tdp.kodeBarang = mb.kodeBarang and tdp.id.noNota = :noNota")
	List<Object[]> findDetailPenjualan(@Param("noNota")String noNota);
	
	@Modifying
	@Query("delete from TrDetailPenjualan tdp where tdp.id.noNota = :noNota")
	void deleteByNoNota(@Param("noNota") String noNota);

}
