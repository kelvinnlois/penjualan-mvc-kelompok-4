package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstCustomer;

public interface MstCustomerDao extends JpaRepository<MstCustomer, String> {
	
	@Query
	("SELECT c,k.namaKota FROM "
			+ "MstCustomer c, MstKota k "
			+ "WHERE c.kodeKota = k.kodeKota "
			+ "AND c.kodeCustomer LIKE %:search%")
	List<Object[]> findCustomer(@Param("search")String search);
	
	@Query(value="select top 1 s.KODE_CUSTOMER from MST_CUSTOMER as s order by s.KODE_CUSTOMER desc",
			nativeQuery= true)
	String findLastKodeCustomer();

}
