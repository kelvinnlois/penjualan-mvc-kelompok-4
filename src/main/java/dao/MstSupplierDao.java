package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstSupplier;

public interface MstSupplierDao extends JpaRepository<MstSupplier, String> {
	
	@Query
	(value = "select s.KODE_SUPPLIER, s.NAMA_SUPPLIER, s.ALAMAT_SUPPLIER, s.TELP_SUPPLIER, s.EMAIL_SUPPLIER, k.NAMA_KOTA " + 
			"from dbo.MST_SUPPLIER as s " + 
			"	left join dbo.MST_KOTA as k on s.KODE_KOTA = k.KODE_KOTA", nativeQuery = true)
	List<Object[]> findAllSupplier(); 
	
	
	@Query
	(value = "select s.KODE_SUPPLIER, s.NAMA_SUPPLIER, s.ALAMAT_SUPPLIER, s.TELP_SUPPLIER, s.EMAIL_SUPPLIER, k.NAMA_KOTA " + 
			"from dbo.MST_SUPPLIER as s " + 
			"	left join dbo.MST_KOTA as k on s.KODE_KOTA = k.KODE_KOTA "
			+ "where s.NAMA_SUPPLIER like %:keyword% "
			+ "or k.NAMA_KOTA like %:keyword% "
			+ "or s.KODE_SUPPLIER like %:keyword%", nativeQuery = true)
	List<Object[]> findOneSupplier(@Param("keyword") String keyword);
	
	@Query(value="select top 1 s.KODE_SUPPLIER from MST_SUPPLIER as s order by s.KODE_SUPPLIER desc", nativeQuery= true)
	String findLastKodeSupplier();

}
