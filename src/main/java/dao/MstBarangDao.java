package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstBarang;

public interface MstBarangDao extends JpaRepository<MstBarang, String> {
	
	@Query
	("SELECT b,s.namaSupplier FROM "
			+ "MstBarang b, MstSupplier s "
			+ "WHERE b.kodeSupplier = s.kodeSupplier "
			+ "AND b.namaBarang LIKE %:search%")
	List<Object[]> findProduct(@Param("search")String search);
	
	@Query(value="select top 1 s.KODE_BARANG from MST_BARANG as s order by s.KODE_BARANG desc",
			nativeQuery= true)
	String findLastKodeBarang();

}
