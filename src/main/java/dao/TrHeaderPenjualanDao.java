package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.TrHeaderPenjualan;

public interface TrHeaderPenjualanDao extends JpaRepository<TrHeaderPenjualan, String>{

	
	@Query("select thp, cus.namaCustomer, kar.namaKaryawan from TrHeaderPenjualan thp, "
			+ "MstCustomer cus, MstKaryawan kar "
			+ "where thp.kodeCustomer = cus.kodeCustomer and thp.kodeKaryawan = kar.kodeKaryawan")
	List<Object[]> findAllHeaderPenjualan();
	
	
	@Query("select thp, cus.namaCustomer, kar.namaKaryawan from TrHeaderPenjualan thp, "
			+ "MstCustomer cus, MstKaryawan kar "
			+ "where thp.kodeCustomer = cus.kodeCustomer and thp.kodeKaryawan = kar.kodeKaryawan "
			+ "and thp.noNota like %:noNota%")
	List<Object[]> findOneHeaderPenjualan(@Param("noNota")String noNota);
	
	@Modifying
	@Query("delete from TrHeaderPenjualan thp where thp.noNota = :noNota")
	void deleteByNoNota(@Param("noNota")String noNota);

}
