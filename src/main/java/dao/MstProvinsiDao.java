package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstProvinsi;

public interface MstProvinsiDao extends JpaRepository<MstProvinsi, String> {

	@Query
	(value = "select KODE_PROVINSI, NAMA_PROVINSI from dbo.MST_PROVINSI", nativeQuery = true)
	List<Object[]> findAllProvinsi();
	
	@Query
	(value = "select KODE_PROVINSI, NAMA_PROVINSI from dbo.MST_PROVINSI "
			+ "where NAMA_PROVINSI like %:cari% or KODE_PROVINSI like %:cari%", nativeQuery = true)
	List<Object[]> findOneProvinsi(@Param("cari") String cari);

	
	@Query(value="select top 1 k.KODE_PROVINSI from MST_PROVINSI as k order by k.KODE_PROVINSI desc", nativeQuery= true)
	String findLastKodeProvinsi();
	
	
//	@Modifying
//	@Query(value = "  select * from dbo.MST_PROVINSI as p order by p.NAMA_PROVINSI asc", nativeQuery=true)
//	List<MstProvinsi> findAll();
	
	
	List<MstProvinsi> findAllByOrderByNamaProvinsiAsc();
}
