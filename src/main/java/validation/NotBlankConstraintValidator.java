package validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotBlankConstraintValidator implements ConstraintValidator<NotBlank, String>{

	private String blank;
	
	@Override
	public void initialize(NotBlank constraintAnnotation) {
		blank = constraintAnnotation.value();
	}

	@Override
	public boolean isValid(String inputString, ConstraintValidatorContext context) {
		if(inputString == null) {
			return true;
		}
		if(inputString.length() != 0 && inputString.trim().equals("")) {
			return false;
		}
		return true;
	}

}
