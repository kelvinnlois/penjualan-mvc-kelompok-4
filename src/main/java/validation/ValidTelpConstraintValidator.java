package validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidTelpConstraintValidator implements ConstraintValidator<ValidTelp, String>{

	private String[] prefixes;
	
	@Override
	public void initialize(ValidTelp constraintAnnotation) {
		prefixes = constraintAnnotation.value();
		
	}

	@Override
	public boolean isValid(String inputString, ConstraintValidatorContext context) {
		boolean result = false;
		if(inputString == null) {
			result = true;
		} else {
			for (String tempPrefix : prefixes) {
	            boolean isPrefixMatch = inputString.startsWith(tempPrefix);
	            // if we found a match then break out of the loop
	            if (isPrefixMatch && tempPrefix.equals("+628")) {
	            	String tail = inputString.substring(4, inputString.length());
	            	if(tail.length() == 10 && tail.matches("[0-9]+")) {
		            	result = true;	    
		            	break;
	            	}
	            } else if (isPrefixMatch && tempPrefix.equals("0")) {
	            	String tail = inputString.substring(1,  inputString.length());
	            	if(inputString.substring(1, 2).equals("8") && tail.length() == 11 && tail.matches("[0-9]+")) {
	            		result = true;	    
		            	break;
	            	} else if (!inputString.substring(1, 2).equals("8") && (tail.length() == 9 || tail.length() == 10 ) && tail.matches("[0-9]+")) {
	            		result = true;	    
		            	break;
	            	}
	            }
            }
		}
		return result;
	}
	
	
}
